----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test the Proc macros.

----------------------------------------------------------------------------

<'

package vlab_utils;

define DEBUG_MACROS;
import e/vlab_util_proc_macros;

-- Macro we need for one of the tests
define <test_procs_gen_times'exp> "gen_times\(<exp>\)" as {lambda (n: uint) { result = n*<exp> }};

extend sys {
   
   !l1: vlab_proc_s;
   !l2: vlab_proc_s of (bool);
   
   run() is also {
      
      ----------------------------------------------------------------------
      -- Create two Procs to calculate parity of a value; l1 prints the result, 
      -- l2 returns a boolean.
      l1 = lambda (a: uint) { outf( (a%2==0) ? "even":"odd ") };
      l2 = lambda (a: uint) { result = (a%2==0) ? TRUE:FALSE };
      
      var xl: list of uint[1..100];
      gen xl keeping { it.size() in [5..10] };
      out(" # | val | l2   | l1   |\n------------------------");
      for each in xl {        
         var parity: string = yield l2 with (it) ? "even": "odd";
         outf("%2d | %3d | %4s | ", index, it, parity);  yield l1 with (it); out(" |");
         check that parity == ((it%2==0) ? "even" : "odd") else dut_error("Proc macro test #1 failed");
      };
      
      ----------------------------------------------------------------------
      -- Check argument parsing
      var l3: vlab_proc_s of (uint(bits:22)) = lambda (p1: int, p2: string, p3: list of bool = {}) { result = p1 };
      print l3;
      check that l3.arity == 2 else  dut_error("Proc macro test #2 failed");
      check that yield l3 with (3, "foo") == 3 else  dut_error("Proc macro test #2 failed");
      
      var l4: vlab_proc_s of (uint(bits:22)) = lambda (p1: int, p2: string = "bar", p3:  list of bool = {}) { result = p1 };
      print l4;
      check that l4.arity == 1 else  dut_error("Proc macro test #2 failed");
      check that yield l4 with (4, "foo") == 4 else  dut_error("Proc macro test #2 failed");
      check that yield l4 with (4) == 4 else  dut_error("Proc macro test #2 failed");
     
      var l5: vlab_proc_s of (uint(bits:22)) = lambda (p1: int, p2: string, p3:  list of bool) { result = p1 };
      print l5;
      check that l5.arity == 3 else dut_error("Proc macro test #2 failed");
      check that yield l5 with(5, "foo", {FALSE;TRUE}) == 5 else  dut_error("Proc macro test #2 failed");
      
      var l6: vlab_proc_s of (uint(bits:22)) = lambda (p1: int=6, p2: string = "bar", p3:  list of bool = {}) { result = p1 };
      print l6;
      check that l6.arity == 0 else dut_error("Proc macro test #2 failed");
      check that (yield l6) == 6 else dut_error("Proc macro test #2 failed");
        
      ----------------------------------------------------------------------
      -- Example from Ruby class docu 
      var times3: vlab_proc_s of (uint) = lambda (n: uint) { result = n*3 };
      var times5: vlab_proc_s of (uint) = lambda(n: uint) { result = n*5 };
      
      check that (yield times3 with(12) == 36) else dut_error("Proc macro test #3 failed");
      check that (yield times5 with(5) == 25) else dut_error("Proc macro test #3 failed");
      check that (yield times3 with(yield times5 with (4)) == 60) else dut_error("Proc macro test #3 failed");
      
      -- same but use macro for lambda definition (makes it resemble the Ruby doc)
      times3 = gen_times(3);
      times5 = gen_times(5);
      out(yield times3 with(12));
      out(yield times5 with(5));
      out(yield times3 with(yield times5 with (4)));
      
      ----------------------------------------------------------------------
      -- Use a string for the code block of the Proc
      var code: string = "{result = x * x}";
      var square: vlab_proc_s of (uint) = lambda_str(x: uint) code;
      print square;
      check that (yield square with (4)) == 16 else dut_error("Proc macro test #4 failed");
      var square2: vlab_proc_s of (uint) = lambda_str(x: uint) { code}; // alternative syntax
      check that square.block == square2.block else dut_error("Proc macro test #4 failed");
   };
};

----------------------------------------------------------------------
-- Example that could be part of an instruction set simulator (iss)

type opcode_t: [LOAD_R0, ADD_C]; // possible opcodes
type data_t: uint(bits: 8);      // data-type for the ISS

-- A simple register model
struct registers_s {
   !r0: data_t;
   !ra: data_t;
   display() is { outf("r0=%03d | ra=%03d\n", r0, ra ) };
};

-- This holds an opcode together with an argument and the code for
-- instruction execution.
struct instruction_s {
   
   op    : opcode_t; // defined earlier as enum [LOAD_R0, ADD_C]
   val   : data_t;
   regs  : registers_s;   
   !code : vlab_proc_s of (data_t);
};

-- Define two instructions to begin with
extend LOAD_R0 instruction_s {
   post_generate() is also {
      code = lambda(regs: registers_s, in: data_t) { regs.r0 = in; result = regs.r0 };
   };
};
extend ADD_C instruction_s {
   post_generate() is also {
      code = lambda(regs: registers_s, in: data_t) { regs.ra = regs.r0+in; result = regs.ra };
   };
};

unit iss_u {

   -- List of random instructions
   instructions: list of instruction_s;
   keep soft instructions.size() == 8;
   keep for each in instructions {
      .regs == regs;
   };
   
   -- The register set instance
   regs: registers_s;
   
   -- This is the main loop of the ISS, executing all instructions in the list
   start() is {
      for each in instructions {
         out("next instruction: ", it.op.as_a(string), " with val = ", dec(it.val));
         compute yield it.code with (regs, it.val);
         regs.display();
      };
   };
};

-- Now let's create a new instruction to swap the content of r0, ra
extend opcode_t: [SWAP];

extend SWAP instruction_s {
   post_generate() is also {
      code = lambda(regs: registers_s, in: data_t=0) { 
         var s: data_t = regs.ra; regs.ra = regs.r0; regs.r0 = s; result = regs.r0 
      };
   };
};

-- Go!
extend sys {
   iss: iss_u is instance;
   run() is also { iss.start() };
};
  
----------------------------------------------------------------------
-- Example that creates a generic sorter (passing Proc objects)

type compare_result_t: int[-1..1];

struct sorter_s {
   
   !strings: list of string;
   
   bubble_sort(compare: vlab_proc_s of (compare_result_t) = lambda (s1: string, s2: string) { 
            result = 0; // even
            if pack(packing.low, s1)[7:0] < pack(packing.low, s2)[7:0] { result = -1; } 
            else if pack(packing.low, s1)[7:0] > pack(packing.low, s2)[7:0] { result = 1;  } 
         }
      ) is {
      
      var idx : int;
      var swap: bool = TRUE;
      while swap {
         idx = 0;
         swap = FALSE;
         while (idx <= max(0, strings.size()-2)) {
            if (yield compare with (strings[idx], strings[idx+1])) == 1 {
               var temp: string = strings[idx];
               strings[idx] = strings[idx+1]; strings[idx+1] = temp;
               swap = TRUE;
            };
            idx += 1;
         };
      };
   };
};

extend sys {
   
   my_sorter: sorter_s;
                                                          
   run() is also {
      var my_strings: list of string = {"fox"; "unicorn"; "baboon"};
      
      -- sort by string length
      var by_length: vlab_proc_s of (compare_result_t) = lambda (s1: string, s2: string) { 
         result = 0; // even
         if str_len(s1) < str_len(s2)      { result = -1; } 
         else if str_len(s1) > str_len(s2) { result =  1; } 
      };
      
      my_sorter.strings = my_strings;
      print my_sorter.strings;
      
      my_sorter.bubble_sort(by_length); // use custom sorting
      print my_sorter.strings;
      
      my_sorter.bubble_sort();          // use default sorting
      print my_sorter.strings; 
   };
};
'>
