----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test times and each macro

----------------------------------------------------------------------------
<'

extend sys {

   run() is also {
      var times_i: int;
      5.times {
        print it;
      };
      
      (2+1).times { print it*2; times_i+=1;};
      
      check that times_i == 3 else
        dut_error("times macro does not work");
   };
};

'>
