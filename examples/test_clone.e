----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test

----------------------------------------------------------------------------
<'

import  e/vlab_util_top;

type kind_t: [APPLE, RED, EXPECTATIONS];

struct base_s {
   name  : string;
   kind  : kind_t;
   value : int[-4..4];

};

struct drvd0_s like base_s {
   keep soft kind == EXPECTATIONS;
   l[4]  : list of int;
};

type drvd0_s is cloneable;

extend sys {
   
   
   ----------------------------------------------------------------------

   a: drvd0_s;
   b: base_s;
   
   run() is also {
      //var c: drvd0_s = b.clone_as(drvd0_s);
      
      print a;
      print b;
      //print c;
      //print deep_compare(b, c, 100);
     
   };
   ----------------------------------------------------------------------
};

'>
