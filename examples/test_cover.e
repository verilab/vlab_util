----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test coverage macro

----------------------------------------------------------------------------
<'

extend sys {

   event cover_e;

   myTime  : time;
   myUint64: uint(bits:64);
   t1: time;
   t2: time;
   t3: time;
   filter: bool;

   cover cover_e is {
      vlab_cov_item myTime using min = 500 ns, max = 1000 ns, num_of_buckets = 2;
      vlab_cov_item myUint64 using min = 0x0, max = 0x800, num_of_buckets = 2;
      vlab_cov_item t1 using min = 100 ns, max = 200 ns;
      vlab_cov_item t2 using min = 0x4000_0000_0000, max = 0x8000_0000_0004, num_of_buckets = 4;
      vlab_cov_item t3 using min = 100 ns, max = 200 ns, when = (!filter);
   };

   run() is also {
      myTime = 0 ns; myUint64 = 0x1; t1 = 1 ns; t2 = 1 ns; t3 = 1 ns; filter = TRUE;
      emit cover_e;
      myTime = 1000 ns; myUint64 = 0x400; t1 = 1 us; t2 = 1 us; t3 = 1 us; filter = FALSE;
      emit cover_e;
      myTime = 500 ns;
      emit cover_e;
      myTime = 1001 ns;
      emit cover_e;
   };

};

'>
