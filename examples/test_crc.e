----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test CRC macros

----------------------------------------------------------------------------
<'

import vlab_util/e/vlab_util_crc_macros;

create_crc_struct crc_32_s;

extend sys {
   crc_32 : crc_32_s;
    
   create_crc_mthd crc_32_m;

   run() is also {
       var msg : list of uint(bits:8) = { 0x31; 0x32; 0x33; 0x34; 0x35; 0x36; 0x37; 0x38; 0x39 };
       print crc_32.calc(msg) using radix=hex;
       print crc_32_m(msg) using radix=hex;
       print msg.crc_32_flip(0, msg.size()) using radix=hex;
       
       assert crc_32.calc(msg) == msg.crc_32_flip(0, msg.size());
   };
};

'>
