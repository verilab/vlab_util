----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Manifest of all tests

----------------------------------------------------------------------------
<'

import e/vlab_util_top;

import examples/test_cover;
import examples/test_delete_all;
import examples/test_times;
import examples/test_misc;
import examples/test_mixin;
import examples/test_procs;

'>
