----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test delete_all macro

----------------------------------------------------------------------------
<'

extend sys {

   run() is also {
      var l1: list of int = {1;2;3;4;-1;-2;5;-5;8};
      var l2: list of int = {1;2;3;4;5;8};
      
      l1.delete_all(it < 0);
      check that l1.is_a_permutation(l2) else
        dut_error("delete_all does not work");
   };
};

'>
