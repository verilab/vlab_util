----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Simple command-line calculator to demonstrate the Proc 
             anonymous method objects.

----------------------------------------------------------------------------

<'

package vlab_utils;

import vlab_util/e/vlab_util_proc_macros;

extend sys {
   gets(s:string): string is foreign dynamic C routine gets;
   
   run() is also {
      -- Need to pre-allocate some memory (note: not protected against overflow in gets())
      var inp: string = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
      var calc: vlab_proc_s of (real);
      
      out("<command-line calculator as demo for Procs>");
      out("<excepts arithmetic expressions like 5/(3.6*1.2)>");
      
      while TRUE {
         outf("<enter numerical expression, blank to exit>");
         compute gets(inp);
         if not str_empty(inp) {
            calc = lambda_str() append("result = ", inp);
            out(yield calc);
         } else {
             out("<exiting>"); break;
         };
      };
   };
};
'>
