----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Example implementation of USB scrambler using the CRC macros.
   cf. "Universal Serial Bus 3.1 Specification, Revision 1.0", B.1 Data Scrambling

----------------------------------------------------------------------------
<'

import vlab_util/e/vlab_util_crc_macros;

extend sys {
    create_crc_mthd usb_next_lfsr using width=16,poly=0x0039,init=0xFFFF,refin=FALSE,refout=FALSE,xorout=0x0000;

    run() is also {
        var dummy_msg : list of uint(bits: 8) = { 0 };
        var lfsr : uint(bits: 16) = 0xFFFF;
        
        var lfsr_seq  : list of uint(bits: 16);
        var scrbl_seq : list of uint(bits: 8);
        
        for i from 0 to 127 {
            var next_byte : uint(bits: 8) = 0; // this would be the byte to be scrambled
            
            lfsr_seq.add(lfsr);
            scrbl_seq.add(%{pack(packing.high,lfsr)} ^ next_byte);
            
            // Generate CRC for a dummy message consisting of a single 0-byte
            lfsr = usb_next_lfsr(dummy_msg, lfsr);
        };

        print lfsr_seq using radix=hex;
        print scrbl_seq using radix=hex;
    };
};

'>
