----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test mixin macros

----------------------------------------------------------------------------
<'

mixin hello_world {
  my_hello() is undefined;
};

extend_mixin hello_world {
  another_hello() is {out("Test test test!")};
};

struct my_s1 {
  mixin hello_world;
  
  my_hello() is {out("A hello from my_s1!")};
};

struct my_s2 {
  mixin hello_world;
  
  my_hello() is {out("A hello from my_s2!")};
};

extend sys {

   run() is also {
      var my_inst1: my_s1 = new;
      var my_inst2: my_s2 = new;
      
      my_inst1.my_hello();
      my_inst2.another_hello();
      print my_inst2.mixes_in(hello_world);
   };
};

'>
