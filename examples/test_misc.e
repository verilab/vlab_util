----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Test other macros

----------------------------------------------------------------------------
<'

import e/vlab_util_top;

type X_t: uint(bits: 3);

struct element_t {
   name : string;
   value: int;
};

extend sys {
   
   ----------------------------------------------------------------------
   -- Ternary expression (macro vlab_util_if_to_ternary) 
   k: int;
   l: bool;
   m: int;
   
   init() is also {
      k = if_expr  (l == TRUE) { k  } else { m%2 };
   };
   
   -- Note: the RHS must be inside of braces.
   keep l == (
      if_expr  (m < 10 and k < m/2) { TRUE } else { FALSE }
      );
   ----------------------------------------------------------------------

   qwaddr: uint; // for testing the macro syntax
   keep soft qwaddr == 0; 
   qsbin : int;
   keep soft qsbin == 0;
   
   run() is also {
      
      ----------------------------------------------------------------------
      -- Hash macros (vlab_add_to_keyed_list and vlab_delete_from_keyed_list)
      var kl: list (key: name) of element_t;
      var new_elem: element_t = new with { .name = "foo"; .value = 3141 };
      
      kl.key("foo") = new_elem;
      
      print kl;
      new_elem = new with { .name = "foo"; .value = 0x100 };
      
      kl.key(new_elem.name) = new_elem;      
      print kl;
      kl.key_del("foo");
      print kl;
      
      var kl2: list (key: it) of uint;
      kl2.key(10) = 10;
      kl2.key(11) = 11;
      print kl2;
      kl2.key_del(10);
      print kl2;
            
      
      ----------------------------------------------------------------------
      -- Size of value/type (macros vlab_util_sizeof and vlab_util_sizeofT)
      var x: X_t = 0b101;
      out("sizeof (x) = ", dec(sizeof (3'b101)));
      out("sizeof (x) = ", dec(sizeof(x)));
      
      print sizeofT(uint);
      print sizeofT(X_t);
      
      
      ----------------------------------------------------------------------
      -- String creation with qs() and qw() 
      -- (macros vlab_util_string_list and vlab_util_string )
      var dict : list of string = qw(fox horse frog);
      print dict;

      var str: string = qs( barfly   );
      out(quote(str));
      
      str = "foobar";
      if str ~~ {"/bar/"; "/foo/"} { out("match")} else { out("mismatch")};
      if str !~~ qw (/bar/ /foo/) { out("mismatch")} else { out("match")};

      var strs: list of string = qw(baboon);
      out(str);
      ----------------------------------------------------------------------
     
   };
};

'>
