----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description:
   Macros to create CRC generators. The first macro creates a
   generator struct, while the second macro creates a single method
   implementing CRC generation.

   The implementation was inspired by pycrc
   (http://www.tty1.net/pycrc/pycrc.html) and verified with the online
   CRC calculator at http://zorc.breitbandkatze.de/crc.html

TODO:
   - Verify widths other than multiples of 8
   - Check whether it makes sense to pass the CRC register by
     reference, so it can be kept for later iterations of a partial
     CRC generation.
   - Implement widths of up to 64bits
   - Profile both implementations 

----------------------------------------------------------------------------

<'
package vlab_utils;

----------------------------------------------------------------------
-- Macro to create a CRC generator class
--
-- Synopsis:
--   create_crc_struct <name> [using (poly=<exp>|width=<exp>|init=<exp>|refin=<exp>|refout=<exp>|xorout=<exp>),...]
-- 
-- Description:
--   Declares a struct <name> which implements table based CRC
--   generation via a single public API method calc(). The method
--   processes a list of bytes, together with an optional initial CRC
--   register value and returns the resulting CRC code (which is not
--   necessarily the same as the final CRC register value).
--
--   The macro accepts several optional parameters. The default set of
--   parameters implements a generator that returns the same result as
--   the crc_32_flip() list pseudo-method in /e/.
--
--   width (uint)
--       The number of significant bits in the CRC Polynomial, excluding
--       the most significant 1. This will also be the number of bits in
--       the final CRC result.
--   
--   poly (uint)
--       The unreflected polynomial of the CRC algorithm, without its
--       most significant bit (common form).
--   
--   refin (bool)
--       Reverse the bit-order of message octets before processing them.
--   
--   init (uint)
--       The initial value of the CRC register.
--   
--   refout (bool)
--       Reverse the bit-order of the final CRC result. This operation
--       takes place before XOR-ing the final CRC value with the xorout
--       parameter.
--   
--   xorout (uint)
--       A value which will be XOR-ed to the final CRC value. 
--
--
-- Examples:
--   create_crc_struct crc_32;
--   create_crc_struct crc_ccitt using width=16, poly=0x1021, init=0xFFFF, xorout=0, refin=FALSE, refout=FALSE
--
--   extend sys {
--       crcgen : crc_ccitt;
--
--       run() is also {
--           var msg : list of uint(bits: 8) = { 0x31; 0x32; 0x33; 0x34; 0x35; 0x36; 0x37; 0x38; 0x39 };
--           var crc : uint(bits: 16);
--
--           print crcgen.calc(msg) using radix=hex;
--       };
--   };

define <vlab_crc_struct'statement> "create_crc_struct <name>[ using <any>]" as computed {
    var seen_poly   : bool;
    var seen_width  : bool;
    var seen_init   : bool;
    var seen_refin  : bool;
    var seen_refout : bool;
    var seen_xorout : bool;
    
    // Default to same param set as crc_32_flip() list pseudo method
    var width  : uint = 32;
    var poly   : uint = 0x04C11DB7;
    var init   : uint = 0xFFFFFFFF;
    var refin  : bool = TRUE;
    var refout : bool = TRUE;
    var xorout : uint = 0xFFFFFFFF;

    var options : list of string;
    
    if not str_empty(<any>) {
        options = str_split(<any>, "/ *, */");
    
        for each (opt) in options {
            compute opt !~ "/^(\w+) *= *(.*)$/";
            
            case $1 {
                "poly":   { if not seen_poly   { poly   = ($2).as_a(uint); seen_poly   = TRUE }};
                "width":  { if not seen_width  { width  = ($2).as_a(uint); seen_width  = TRUE }};
                "init":   { if not seen_init   { init   = ($2).as_a(uint); seen_init   = TRUE }};
                "refin":  { if not seen_refin  { refin  = ($2).as_a(bool); seen_refin  = TRUE }};
                "refout": { if not seen_refout { refout = ($2).as_a(bool); seen_refout = TRUE }};
                "xorout": { if not seen_xorout { xorout = ($2).as_a(uint); seen_xorout = TRUE }};
                default:  { reject_match() };
            };
        };
    };

    // reasonable defaults
    if (    seen_refin and not seen_refout) { refout=refin };
    if (not seen_refin and     seen_refout) { refin =refout };

    if refin {
        init = %{pack(packing.high, init)[0..width-1]};
    };
    
    result = appendf("struct %s {\n", <name>);
    result = appendf("%s    private !crc_table[256] : list of uint(bits: %d);\n\n", result, width);
    
    // ______ calc() ______
    result = appendf("%s    calc(lob: list of uint(bits: 8), crc: uint(bits: %d)=%d): uint(bits: %d) is {\n", result, width, init, width);
    result = appendf("%s        for each (b) in lob {\n", result);
    if refin {
        result = appendf("%s            crc = crc_table[(crc ^ b) & 0xFF] ^ (crc>>8);\n", result);
    } else {
        result = appendf("%s            crc = crc_table[((crc>>(%d-8)) ^ b) & 0xFF] ^ (crc<<8);\n", result, width);
    };
    result = appendf("%s        };", result);
    if (refin == refout) { result = appendf("%s        result = crc;\n", result); }
    else                 { result = appendf("%s        result = revbits(crc);\n", result); };
    if xorout != 0 { result = appendf("%s        result ^= %d;\n", result, xorout); };    
    result = appendf("%s    };\n\n", result);
    
    // ______ revbits() ______
    result = appendf("%s    private revbits(b : uint(bits: %d)): uint(bits: %d) is {\n", result, width, width);
    result = appendf("%s        result = %%{pack(packing.high, b)}\n", result);
    result = appendf("%s    };\n\n", result);

    // ______ rev8() ______
    result = appendf("%s    private rev8(b : uint(bits: 8)): uint(bits: 8) is {\n", result);
    result = appendf("%s        result = %%{pack(packing.high, b)}\n", result);
    result = appendf("%s    };\n\n", result);

    // ______ maketable() ______
    result = appendf("%s    private maketable(table : list of uint(bits: %d)) is {\n",result, width);
    result = appendf("%s        var i : uint(bits: 8);\n", result);
    result = appendf("%s        var r : uint(bits: %d);\n", result, width);
    result = appendf("%s        for i from 0 to 255 do {\n", result);
    if refin {
        result = appendf("%s            r = rev8(i)<<(%d);\n", result, width-8);
    } else {
        result = appendf("%s            r = i<<(%d);\n", result, width-8);
    };
    result = appendf("%s            for j from 0 to 7 do {\n", result);
    result = appendf("%s                if (r[%d:%d] == 1) {\n", result, width-1, width-1);
    result = appendf("%s                    r = (r<<1) ^ %d;\n", result, poly);
    result = appendf("%s                } else {;\n", result);
    result = appendf("%s                    r = (r<<1);\n", result);
    result = appendf("%s                };\n", result);
    result = appendf("%s            };\n", result);
    if refin {
        result = appendf("%s            r=revbits(r);\n", result);
    };
    result = appendf("%s            table[i] = r;\n", result);
    result = appendf("%s        };\n", result);
    result = appendf("%s    };\n\n", result);

    // ______ post_generate() ______
    result = appendf("%s    post_generate () is also {\n", result);
    result = appendf("%s        maketable(crc_table);\n", result);
    result = appendf("%s    };\n\n", result);

    
    result = appendf("%s};\n", result);
};

----------------------------------------------------------------------
-- Macro to create a CRC generator method
--
-- Synopsis:
--   create_crc_mthd <name> [using (poly=<exp>|width=<exp>|init=<exp>|refin=<exp>|refout=<exp>|xorout=<exp>),...]
-- 
-- Description:
--   Declares a method <name> which implements table based CRC
--   generation. The method processes a list of bytes, together with
--   an optional initial CRC register value and returns the resulting
--   CRC code (which is not necessarily the same as the final CRC
--   register value).
--
--   The macro accepts several optional parameters. The default set of
--   parameters implements a generator that returns the same result as
--   the crc_32_flip() list pseudo-method in /e/.
--
--   width (uint)
--       The number of significant bits in the CRC Polynomial, excluding
--       the most significant 1. This will also be the number of bits in
--       the final CRC result.
--   
--   poly (uint)
--       The unreflected polynomial of the CRC algorithm, without its
--       most significant bit (common form).
--   
--   refin (bool)
--       Reverse the bit-order of message octets before processing them.
--   
--   init (uint)
--       The initial value of the CRC register.
--   
--   refout (bool)
--       Reverse the bit-order of the final CRC result. This operation
--       takes place before XOR-ing the final CRC value with the xorout
--       parameter.
--   
--   xorout (uint)
--       A value which will be XOR-ed to the final CRC value. 
--
--
-- Examples:
--   extend sys {
--       create_crc_mthd crc_ccitt using width=16, poly=0x1021, init=0xFFFF, xorout=0, refin=FALSE, refout=FALSE
--
--       run() is also {
--           var msg : list of uint(bits: 8) = { 0x31; 0x32; 0x33; 0x34; 0x35; 0x36; 0x37; 0x38; 0x39 };
--           var crc : uint(bits: 16);
--
--           print crc_ccitt(msg) using radix=hex;
--       };
--   };

define <vlab_crc_mthd'struct_member> "create_crc_mthd <name>[ using <any>]" as computed {
    var seen_poly   : bool;
    var seen_width  : bool;
    var seen_init   : bool;
    var seen_refin  : bool;
    var seen_refout : bool;
    var seen_xorout : bool;
    
    // Default to same param set as crc_32_flip() list pseudo method
    var width  : uint = 32;
    var poly   : uint = 0x04C11DB7;
    var init   : uint = 0xFFFFFFFF;
    var refin  : bool = TRUE;
    var refout : bool = TRUE;
    var xorout : uint = 0xFFFFFFFF;

    var options : list of string;
    
    if not str_empty(<any>) {
        options = str_split(<any>, "/ *, */");
    
        for each (opt) in options {
            compute opt !~ "/^(\w+) *= *(.*)$/";
            
            case $1 {
                "poly":   { if not seen_poly   { poly   = ($2).as_a(uint); seen_poly   = TRUE }};
                "width":  { if not seen_width  { width  = ($2).as_a(uint); seen_width  = TRUE }};
                "init":   { if not seen_init   { init   = ($2).as_a(uint); seen_init   = TRUE }};
                "refin":  { if not seen_refin  { refin  = ($2).as_a(bool); seen_refin  = TRUE }};
                "refout": { if not seen_refout { refout = ($2).as_a(bool); seen_refout = TRUE }};
                "xorout": { if not seen_xorout { xorout = ($2).as_a(uint); seen_xorout = TRUE }};
                default:  { reject_match() };
            };
        };
    };

    // reasonable defaults
    if (    seen_refin and not seen_refout) { refout=refin };
    if (not seen_refin and     seen_refout) { refin =refout };

    if refin {
        init = %{pack(packing.high, init)[0..width-1]};
    };
    
    // ______ generator method ______
    result = appendf("%s    %s(lob: list of uint(bits: 8), crc: uint(bits: %d)=%d): uint(bits: %d) is {\n", result, <name>, width, init, width);
    result = appendf("%s        for each (b) in lob {\n", result);
    result = appendf("%s            var idx : uint;\n", result);
    result = appendf("%s            var tbl : uint;\n", result);
    if refin {
        result = appendf("%s            idx = (crc ^ b) & 0xFF;\n", result);
    } else {
        result = appendf("%s            idx = ((crc>>(%d-8)) ^ b) & 0xFF;\n", result, width);
    };
    
    result = appendf("%s            case idx {\n", result);

    var i : uint;
    var r : uint;
    
    for i from 0 to 255 do {
        if refin {
            r = %{pack(packing.high, i[7:0])};
            r <<= (width-8);
        } else {
            r = i<<(width-8);
        };
        for j from 0 to 7 do {
            if (r[width-1:width-1] == 1) {
                r = (r<<1) ^ poly;
            } else {
                r = (r<<1);
            };
        };
        if refin {
            r=%{pack(packing.high, r[width-1:0])};
        };
        result = appendf("%s                %d: { tbl = %d };\n", result, i, r);
    };
    result = appendf("%s            };\n", result);

    if refin {
        result = appendf("%s            crc = tbl ^ (crc>>8);\n", result);
    } else {
        result = appendf("%s            crc = tbl ^ (crc<<8);\n", result);
    };
    
    result = appendf("%s        };\n", result);
    if (refin == refout) { result = appendf("%s        result = crc;\n", result); }
    else                 { result = appendf("%s        result = %%{pack(packing.high, crc[%d-1:0])};\n", result, width); };
    if xorout != 0 { result = appendf("%s        result ^= %d;\n", result, xorout); };    
    result = appendf("%s    };\n", result);
};

'>
