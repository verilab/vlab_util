----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Macro definitions

----------------------------------------------------------------------------

<'
package vlab_util;

----------------------------------------------------------------------
-- Macro that adds a repetition operator like in Verilog
-- Caveats: expression must be packable (not checked). Room for improvement.
-- Example: 
--    print 3***(2'b10);

extend sn_util {
  vlab_repetition(factor: uint, exp: list of bit): list of bit is {
    var i: uint;
    for i from 1 to (factor) {
      result.add(exp);
    };
  };
};

define <vlab_repetition_op_scalar'exp> "<factor'exp>***\(<rep'exp>\)" as computed {
  result = appendf("util.vlab_repetition(%s, %%{%s})[:]", str_expand_dots(<factor'exp>), str_expand_dots(<rep'exp>));
};

----------------------------------------------------------------------
-- Macro that returns the bit-size of an expression or a type.
-- Caveats: expression must be packable (not checked). Room for improvement.
-- Example: 
--    print sizeof(3'b101);
--    print sizeofT(myT);

extend sn_util {
   -- Returns size (in bits) of a scalar type passed as string, 
   -- or 0 if it is not a scalar (croaks).
   vlab_sizeof_scalar_type(name: string): uint is {
      var t: rf_type = rf_manager.get_type_by_name(name);
      if t is a rf_scalar (sc) {
         result = sc.get_size_in_bits();
      }else {
         warning("vlab_util::vlab_sizeof_scalar_type: Expected scalar-type, got ", quote(name));
         result = UNDEF;
      };
   };
};

define <vlab_util_sizeof'exp> "sizeof\(<exp>\)" as {
   pack(NULL, <exp>).size()
};
define <vlab_util_sizeofT'exp> "sizeofT\(<type>\)" as {
   util.vlab_sizeof_scalar_type("<type>");
};

----------------------------------------------------------------------
-- Macro that provides string list creation with Perl's qw() operator
-- Example: 
--    var dict : list of string = qw(fox horse frog);

define <vlab_util_string_list'exp> "qw(<ELEMENTS><any> ...)" as computed {
   -- extract strings
   var r: list of string = str_split(str_expand_dots(<ELEMENTS>), "/\s\s*/");
   
   -- delete empty strings
   var de: list of int;
   for each in r {
      if str_empty(it) { de.add(index) };
   };
   for each in de.reverse() { r.delete(it) };
   
   -- detect and remove parentheses from first and last element
   if not r.is_empty() {
      if r[0] !~ "/^\(/" or r[r.size()-1] !~ "/\)$/" { 
         reject_match() 
      };
      r[r.size()-1] = str_replace(r.top(), "/\)$/", "");
      r[0]          = str_replace(r[0], "/^\(/","");
   };
   
   -- sanity check - if we see a comma or semicolon, the user probably inserted a wrong delimiter (croak)
   if r.has(it ~ "/,/") { 
      warning(appendf("String list should be separated by spaces, but contains a \",\" - maybe a mistake !?\nMacro was called in module @%s at line %s", 
                      get_current_module().get_name(), get_current_line_num()));
   };
   
   -- return list of quoted strings
   result = "{";
   for each in r { result = append(result, quote(it), ";") };
   result = append(result, "}");
};

----------------------------------------------------------------------
-- Macro that creates a string from unqouted characters (like the 
-- qw() macro above)
-- Example: 
--    keep hprot.hdl_path() == qs ( master0_hprot );

define <vlab_util_string'exp> "qs\(<any>\)" as computed {
   var rs: string= str_expand_dots(<any>);
   result = quote(str_trim(rs));
};

----------------------------------------------------------------------------
-- Macro that provides a new operator "~~" and its negation "!~~" to check 
-- whether a string matches one of the patterns in a list.
-- Example: 
--    if str ~~ {"/bar/"; "/foo/"} { out("match") } else { out("mismatch") };
-- Can be combined with qw() macro:
--    if str ~~ qw(/bar/ /foo/) { out("match")} else { out("mismatch")};

-- Helper function for the operators:
extend sn_util {
   vlab_str_match_in_patterns(match: string, patterns: list of string):bool is {
      for each in patterns {
         if str_match(match,it) { return TRUE };
      };
   };
};

-- The macro code:
define <vlab_util_string_match_in_patterns'exp> "<lhs'exp> ~~ <rhs'exp>" as {
   util.vlab_str_match_in_patterns(<lhs'exp>, <rhs'exp>)
};

define <vlab_util_string_nmatch_in_patterns'exp> "<lhs'exp> !~~ <rhs'exp>" as {
   !util.vlab_str_match_in_patterns(<lhs'exp>, <rhs'exp>)
};

----------------------------------------------------------------------
-- Macro that converts if/else expressions to ternary ones, allowing to
-- e.g. use if/else in constraints (like in SystemVerilog)
-- Example in assignment: 
--     var k: int = if_expr (l == TRUE) { k  } else { k % 2 };
-- Example in constraints:
--     keep l == (
--      if_expr  (m < 10 and k < m/2) { TRUE } else { FALSE }
--      );
-- Note the outer parentheses to waive operator precedence rules 
 
define <vlab_util_if_to_ternary'exp> "if_expr <cond'exp> {<cond_if'exp>;...} else {<cond_else'exp>;...}" as computed {
   result = append("(", str_expand_dots(<cond'exp>), ") ? ");
   
   if (<cond_if'exps> is not empty) {
      result = append(result, "(", str_join(<cond_if'exps>, ") and ("), ")");
   };
   
   result = append(result, " : ");
   
   if (<cond_else'exps> is not empty) {
      result = append(result, "(", str_join(<cond_else'exps>, ") and ("), ")");
   };
   result = append("(", result, ")");
   // out(result); -- DEBUG
};

----------------------------------------------------------------------
-- Macro that allows all elements of a list to be deleted with a certain condition
-- Example: 
--    my_list.delete_all(it < 5);

define <vlab_delete_all_from_list'action> "<list'exp>\.delete_all\(<filter'exp>\)" as {
  for each in (<list'exp>.all_indices(<filter'exp>).reverse()) {
     <list'exp>.delete(it);
  };
};

----------------------------------------------------------------------
-- Macro for a shortcut for a for loop. You can use the variable it to refer to the index
-- Example: 
--    5.times {print it};

define <vlab_repeat_loop'action> "<exp>\.times <block>" as {
  for it from 1 to <exp> <block>;
};

----------------------------------------------------------------------
-- Macro for a Ruby like for each loop
-- Example: 
--    list.each {print it};

define <vlab_list_each'action> "<exp>\.each <block>" as {
  for each in <exp> <block>;
};

----------------------------------------------------------------------
-- Example macro from the slides to let a user extend an enum type
-- automatically with the upcased version of the given string.
-- Example: 
--    vlab_extend_upper myT: new_enum_elem;

define <vlab_extend_enum_upper'statement> "vlab_extend_upper <enum'name>: <elem'name>" as computed {
  result = append("extend ", <enum'name>, ": [", str_upper(<elem'name>), "];");
};

'>
