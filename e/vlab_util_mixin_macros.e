----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Macros to provide Ruby like mixin functionality. The classic
             examples are the comparable and the enumerable mixins. In
             general any struct can be enriched with certain common
             functionality using mixins. They don't need a common base class.
             

----------------------------------------------------------------------------

<'
package vlab_util;

// Hash table entry of a certain mixin
struct vlab_mixin_s {
  name : string;
  mixin: string;
};

// keep Hash table of all defined mixins
extend sn_util {
  vlab_mixins: list (key: name) of vlab_mixin_s;
  
  vlab_mixin_add(name: string, mixin: string) is {
    if vlab_mixins.key_exists(name) {
      error("Mixin ", name, " exists already. Use is only/first/also instead!");
    } else {
      vlab_mixins.add(new vlab_mixin_s with {.name = name; .mixin = append(mixin, ";")});
    };
  };
  
  vlab_mixin_extend(name: string, mixin: string) is {
    if vlab_mixins.key_exists(name) {
      vlab_mixins.key(name).mixin = append(vlab_mixins.key(name).mixin, "\n", mixin, ";");
    } else {
      error("Mixin ", name, " does not exist. Use standard mixin macro first to define mixin!");
    };
  };
};

----------------------------------------------------------------------
-- Macro to define a mixin (similar to a Ruby module but that word is already used in HDL land)
-- Example: 
--     mixin comparable {
--        my_compare_method():int is undefined;
--     };
define <vlab_mixin_declaration'statement> "(<MATCH>mixin <name> <block>)" as computed {
  util.vlab_mixin_add(<name>, <block>);
  result = appendf("\
    extend any_struct {\
      vlab_mixes_in_%s(): bool is {result = FALSE};\
    };\
  ", <name>);
};

----------------------------------------------------------------------
-- Macro to extend the definition of a mixin (similar to splitting a Ruby module across multiple files)
-- Example: 
--     extend_mixin comparable {
--        my_other_method() is undefined;
--     };
define <vlab_mixin_extension'statement> "(<MATCH>extend_mixin <name> <block>)" as computed {
  util.vlab_mixin_extend(<name>, <block>);
};

----------------------------------------------------------------------
-- Macro to import a mixin (similar to a Ruby include but I wanted
-- to keep everything mixin related called mixin). The macro basically
-- copy and pastes the defined code of the mixin into the struct.
-- Example: 
--     struct s {
--       mixin comparable;
--     };
define <vlab_mixin_import'struct_member> "(<MATCH>mixin <name>)" as computed {
  result = appendf("vlab_mixes_in_%s(): bool is only {result = TRUE};", <name>);
  result = append(result, "\n", util.vlab_mixins.key(<name>).mixin);
};

----------------------------------------------------------------------
-- Macro/pseudo method to check if a certain struct imports a certain mixin
-- Example: 
--     if my_s.mixes_in(comparable) {...};
define <vlab_mixin_query'exp> "(<MATCH><exp>\.mixes_in\(<name>\))" as {
  <exp>.vlab_mixes_in_<name>()
};

'>
