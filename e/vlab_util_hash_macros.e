----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description     : Macros to extend the built-in keyed-lists to emulate real
                  hash functionality.

----------------------------------------------------------------------------

<'
package vlab_vlab_util;


----------------------------------------------------------------------
-- Macro to add an element to a keyed list, overwriting possible
-- existing entries (-> ensures unique keys).
-- Example: 
--     var kl: list (key: name) of element_t;
--     var new_elem: element_t = new with { .name = "foo"; .value = 3141 };     
--     kl.key("foo") = new_elem;

define <vlab_add_to_keyed_list'action> "<name>.key\(<key'exp>\)[ ]=[ ]<val'exp>" as computed {
   var rl: list of string;
   var kl : string = str_expand_dots(<name>);
   var key: string = str_expand_dots(<key'exp>);
   var val: string = str_expand_dots(<val'exp>);
   
   rl.add(appendf("var idx: int =  %s.key_index(%s);", kl, key));
   rl.add(append ("if idx != UNDEF {"));
   rl.add(appendf("   %s.delete(idx);", kl));
   rl.add(append ("};"));
   rl.add(appendf("%s.add(%s);",kl, val));
   
   result = appendf("{%s};", str_join(rl, " "));
};

----------------------------------------------------------------------
-- Macro to delete a key/value pair from a keyed-list. No error is
-- emitted if the key does not exist.
-- Example:
--    kl.key_del("foo"); 

define <vlab_delete_from_keyed_list'action> "<name>.key_del\(<key'exp>\)" as computed {
   var rl: list of string;
   var kl : string = str_expand_dots(<name>);
   var key: string = str_expand_dots(<key'exp>);
   
   rl.add(appendf("var idx: int =  %s.key_index(%s);", kl, key));
   rl.add(append ("if idx != UNDEF {"));
   rl.add(appendf("   %s.delete(idx);", kl));
   rl.add(append ("};"));  
   
   result = appendf("{%s};", str_join(rl, " "));
};
'>
