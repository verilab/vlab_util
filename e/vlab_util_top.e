----------------------------------------------------------------------------

    COPYRIGHT (c) 2014, 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Manifest for the vlab_util package

----------------------------------------------------------------------------

<'
package vlab_util;

import vlab_util/e/vlab_util_macros;
import vlab_util/e/vlab_util_hash_macros;
import vlab_util/e/vlab_util_cover_macros;
import vlab_util/e/vlab_util_mixin_macros;
import vlab_util/e/vlab_util_crc_macros;
import vlab_util/e/vlab_util_proc_macros;

'>
