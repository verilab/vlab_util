----------------------------------------------------------------------------

    COPYRIGHT (c) 2014 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Macros to cover integral items with more than 32 bits.

----------------------------------------------------------------------------

<'
package vlab_util;

extend sn_util {
  
  // Check unbound int num against low and high limits. By default only one bin is assumed.
  // If necessary up to MAX_UINT - 1 bins can be selected. The return value means:
  // 0 - num is smaller than low boundary
  // 1... - number of bin the value t falls into between low and high boundaries
  // MAX_UINT - num is higher than high boundary
  vlab_get_cov_range(num: int(bits:*), low: int(bits:*), high: int(bits:*), bins: uint = 1): uint is {
    if num < low {
      return 0;
    } else if num > high {
      return MAX_UINT;  
    } else if num == high {
      return bins;  
    } else {
      if bins > 1 {
        if low == high {
          return 1;
        } else {
          return ((num-low) / ((high - low) / bins)) + 1;
        };
      } else {
        return 1;
      };
    };
  };
};

----------------------------------------------------------------------
-- Macro to add a cover item for a variable of type time
-- Example: 
--     vlab_time_item t using min = 100 ns, max = 200 ns;
--     vlab_time_item t using min = 100 ns, max = 200 ns, num_of_buckets = 4;
--     vlab_time_item t using min = 100 ns, max = 200 ns, num_of_buckets = 4, other_using_options...;
define <vlab_cover_item'cover_item> "(<MATCH>vlab_cov_item <name> using <action>,...)" as computed {
   var min_s: string;
   var max_s: string;
   var min_unit: e_time_units;
   var max_unit: e_time_units;
   var min: int(bits:*);
   var max: int(bits:*);
   var buckets: int = 1;
   var using_number_literals: bool;
   var using_hex_literals   : bool;
   var using_time_literals  : bool;
   var using_l_filtered: list of string;  // remaining using arguments without min, max, num_of_buckets
   var time_units_match: string = str_join(all_values(e_time_units).apply(it.as_a(string)), "|");

   // extract "using" parameters
   for each in (<actions>) {
      if str_match(it, "/^\s*min\s*=\s*(.*)$/") {
         min_s = $1;
      } else if str_match(it, "/^\s*max\s*=\s*(.*)$/") {
         max_s = $1;
      } else if str_match(it, "/^\s*num_of_buckets\s*=\s*(.*)$/") {
         buckets = $1.as_a(int);
      } else {
         using_l_filtered.add(it);
      };
   };
   
   if str_empty(min_s) || str_empty(max_s) {
      error(append("Macro needs at least a min and max variable after the using keyword. ",
                   "Here is what got specified instead: \n", <MATCH>));
   };
   
   // figure out if min/max parameters are integer or time literals.
   // If neither, the user must be using dynamic ranges and passed in a variable.
   try {
      min = min_s.as_a(int(bits:*)); // this will go wrong if not an e recognized number format or time format,
      max = max_s.as_a(int(bits:*)); // if it goes wrong, it will to to the else block of the try action
      using_number_literals = TRUE;
      
      // Test for hex literals
      if str_match(min_s, "/^([0-9]+'[hH]|0[xX])[0-9a-fA-F_]+\s*$/") ||
         str_match(max_s, "/^([0-9]+'[hH]|0[xX])[0-9a-fA-F_]+\s*$/") {
         using_hex_literals = TRUE;
      };
      
      // Test for time literals
      if str_match(min_s, appendf("/ (%s)$/", time_units_match)) {
         min_unit = $1.as_a(e_time_units);
         if str_match(max_s, appendf("/ (%s)$/", time_units_match)) {
            using_time_literals = TRUE;
            max_unit = $1.as_a(e_time_units);
            // calculate specman scaled time values for higher accuracy range calculation
            min = to_specman_scale(min, min_unit);
            max = to_specman_scale(max, max_unit);
            // determine the smaller of the units for the range printing later
            min_unit = min_unit.as_a(int) < max_unit.as_a(int) ? min_unit : max_unit;
         };
      };
   } else {
      using_number_literals = FALSE;
   };
   
   // construct result string, the actual cover item
   result = append(
               "item ",
               <name>,
               ": uint = util.vlab_get_cov_range(", <name>,
               ", ",  min_s,
               ", ",  max_s,
               ") using ranges = {\n"
            );
   // Add bin which checks if value is lower than min boundary
   result = append(result, "range([0], \"", <name>, " lower than min boundary (", min_s, ")\" , UNDEF,  1);\n");

   // Add bins depending on number of buckets
   if using_number_literals {
      for range_i from 1 to buckets {
         var low : int(bits:*);
         var high: int(bits:*);
         var low_s : string;
         var high_s: string;
         low  = min + ((max - min) / buckets) * (range_i - 1);
         high = min + ((max - min) / buckets) *  range_i      - (range_i == buckets ? 0 : 1);
         if using_time_literals {
            low_s  = appendf("%0d %s", from_specman_scale(low , min_unit), min_unit);
            high_s = appendf("%0d %s", from_specman_scale(high, min_unit), min_unit);
         } else {
            low_s  = using_hex_literals ? hex(low)  : dec(low);
            high_s = using_hex_literals ? hex(high) : dec(high);
         };
         result = append(result, "range([", range_i,"], \"", <name>, " within boundaries (", low_s, ", ", high_s, ")\"  , UNDEF,  1);\n");
      };
   } else {
      for range_i from 1 to buckets {
         result = append(result, "range([", range_i,"], \"", <name>, " within boundaries (range ", range_i,")\"  , UNDEF,  1);\n");
      };
   };
   
   // Add bin which checks if value is higher than max boundary
   result = append(result, "range([MAX_UINT], \"", <name>, " higher than max boundary (", max_s, ")\", UNDEF,  1);\n",
               "}"
            );
   
   // Append any remaining using arguments
   if !using_l_filtered.is_empty() {
      result = append(result,
                  ", ",
                  str_join(using_l_filtered, ",")
               );
   };
   result = append(result, ";");
};

'>
