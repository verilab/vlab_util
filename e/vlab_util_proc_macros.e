----------------------------------------------------------------------------

    COPYRIGHT (c) 2015 by Verilab GmbH

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

----------------------------------------------------------------------------

Description: Macro and struct definitions that add anonymous method objects
             ("Procs") to the e language. They resemble the Ruby Procs but are
             less powerful. E.g. the Proc blocks can only use variables that are 
             passed by the Proc call, not from other context (except sys of course). 
             Default values for inputs are possible though.

             Note: uses macro rf_type(<exp>):rf_type which is sort of unofficial.
----------------------------------------------------------------------------

<'

package vlab_utils;

----------------------------------------------------------------------------
-- Base struct for Proc object.
-- Not for user.
-- 
-- It uses rf_value_holder variables in sn_util to store params+return value/
-- and replace strings with reflection objects -> allows to pass more than just numeric values in Proc call
struct vlab_proc_base_s {
   -- Fields that must be initialized upon instance creation
   const !block              : string;
   const !has_args           : bool;
   const !is_value_returning : bool;
   !arg_names                : list of string;
   !arg_types_and_defaults   : list of string;
   
   -- Calculated fields
   !arg_types                : list of string;
   !ret_type                 : string;
   !arity                    : uint; // number of mand. arguments
   
   -- Assemble the call-string for specman() method; it depends on the number of arguments and whether 
   -- this is value-returning or not.
   call_str(): string is {
      -- Add code to declare result variable
      if is_value_returning {
         result = append("var result:", ret_type,";");
      };
      
      -- Add code to declare variables for each parameter and pull the values from global holders
      if has_args {
         var n_args: int = 0;
         for each (arg_name) in arg_names {
            if util.vlab_proc_container.input.exists(index) {
               result = append(result, append("var ", arg_name, ":", arg_types[index], "= util.vlab_proc_container.input[",index,"].get_value().unsafe();"));
               n_args += 1;
            } else {
               result = append(result, append("var ", arg_name, ":", arg_types_and_defaults[index], ";"));
            };
         };
         assert n_args >= arity else error("Proc has ", dec(arity), " mandatory argument(s), but less found in Proc call");
      };
      
      -- Add block to call-string
      result = append(result, block, ";");
      
      -- Add code to transfer result to global holder
      if is_value_returning {
         result = append(result, "util.vlab_proc_container.set_output(result.unsafe());")
      };
      //DEBUG:out("call_str >>> ", result);
   };
   
   -- Call Proc object, no return values. Parameters can only be inputs.
   void_call(params: list of rf_value_holder={}) is {
      assert params.size() <= arg_names.size() else error("Number of parameters (", dec(params.size()), ") in Proc call exceeds number in Proc definition (", arg_names.size(), ").");
      util.vlab_proc_container = new with { .input = params };
      specman(call_str());
   };
   
   -- Setup method
   calc_props() is {
      -- Extract the argument types from the definition (which might contain a default value)
      for each (arg) in arg_types_and_defaults {
         var arg_type := str_split(arg, "/\s*=\s*/")[0];
         arg_types.push(arg_type);
      };
      
      -- Calculate number of mandatory arguments (i.e. no default value given)
      var n: int = arg_types_and_defaults.first_index(str_match(it, "/\s*=\s*/"));
      if (n == UNDEF) {
         arity = arg_names.size();
      } else {
         arity = n;
      };
   };
};

----------------------------------------------------------------------------
-- Struct that represents an anonymous method (Proc). Use this to declare a 
-- Proc object.
-- The template type is the return value of the Proc. For non-value-returning
-- methods, nothing has to be specified. 
--
-- Examples:
--    var x: vlab_proc_s of (bool); // Proc returns a boolean
--    var y: vlab_proc_s;           // Proc is non-value returning or uses default type uint
template struct vlab_proc_s of (<ret'type>=uint) like vlab_proc_base_s { 
   private !type_plate : <ret'type>;// dummy to retrieve template type
   private !ret_rft    : rf_type;   // the type of this template as string
   
   calc_props() is also {
      try {
         var rfield := rf_manager.get_struct_of_instance(me).get_field("type_plate");
         ret_rft     = rfield.get_type();
         -- Get the type of this template as string
         ret_type    = ret_rft.get_name();
      } else { error("Error during initiaization of Proc (reflection interface)") };
   };
   
   -- Call Proc object, return result to caller via the global holder. Parameters can only be inputs.
   non_void_call(params: list of rf_value_holder={}):<ret'type> is {
      assert params.size() <= arg_names.size() else error("Number of parameters (", params.size(), ") in call exceeds number in Proc definition (", arg_names.size(), ").");
      util.vlab_proc_container = new with {
         .input      = params;
         .output_rft = ret_rft;
      };
      specman(call_str());
      result = util.vlab_proc_container.output.get_value().unsafe();
   };
};

----------------------------------------------------------------------------
-- Macro that calls a non-value-returning Proc object, passing the given input
-- parameters (if any) to the Proc object. The input must be literal values.
-- Examples:
--    yield x with(1,2);
--    yield y;

define <vlab_proc_call'action> "yield <obj'exp>[ with[ ]\(<args'name>,...\)]" as computed {
   result = append(<obj'exp>, ".void_call({");
   for each in (<args'names>) {
      result = append(result, appendf("rf_type(%s).create_holder(%s.unsafe());", str_expand_dots(it), str_expand_dots(it)))
   };
   result = append(result, "})");
};

----------------------------------------------------------------------------
-- Macro that calls a value-returning Proc object, passing the given input
-- parameters (if any) to the Proc object. The input must be literal values.
-- Examples:
--    var k:int = yield y;
--    out("y yields: ", yield y with (6, FALSE));
-- Note: for the parser it might be necessary to enclose the yield expression in parentheses

define <vlab_proc_call'exp> "yield <obj'exp>[ with[ ]\(<args'name>,...\)]" as computed {
   result = append(<obj'exp>, ".non_void_call({");
   for each in (<args'names>) {
      result = append(result, appendf("rf_type(%s).create_holder(%s.unsafe());", str_expand_dots(it), str_expand_dots(it)))
   };
   result = append(result, "})");
};

----------------------------------------------------------------------------
-- Macro that creates a vlab_proc_s object given an optional return type and optional input arguments. 
-- Arguments without a default are treated as mandatory for the call. The code block can only
-- use variables passed as arguments to the Proc. An alternative syntax exist to specify the code block
-- as a string.
-- For value-returning code blocks, the "result" variable must be assigned to, otherwise it will not be
-- recognized as such.
--
-- Examples:
--     var x: vlab_proc_s = lambda(a: int = 5, b: int=999) { out("a+b=", a+b) }; // non-value returning
--     var y: vlab_proc_s of (bool) = lambda(a: int) { result = a<0 ? TRUE: FALSE }; // returns a boolean
--
--     If expression is a string:
--     var code: string = "result = a*a";
--     var z: vlab_proc_s of (real) = lambda_str(a: real = 0) code;

define <vlab_proc_def'exp> "(<MATCH>lambda[_str][[ ]\(<args'name>,...\)]<block'any>)" as computed {
   var rl: list of string;
   var is_string: bool = FALSE;
   
   var expression: string = str_trim(str_expand_dots(<block'any>));
   
   -- Check if code block is a string; remove curlies if any
   if str_match(<MATCH>, "/^lambda_str/") { 
      expression = str_replace(expression,  "/^\s*{(.*)}$/", "\1");
      is_string = TRUE 
   };
   
   if str_empty(expression) { reject_match() };
   
   -- Parse the method argument list
   var i_names: list of string;
   var i_types: list of string;
   for each (arg) in (<args'names>.apply(str_expand_dots(it))) {
      var args: list of string = str_split(arg, ":");
      if args.size()>2 { args = {args[0];str_join(args[1..], ":")} };
      assert args.size() == 2 else error(appendf("Invalid input parameter definition in macro call (trouble parsing \"%s\") in module %s at line %d.", arg, get_current_module().get_name(), get_current_line_num()));
      args = args.apply(str_trim(it));
      i_names.add(args[0]); i_types.add(args[1]);
   };

   -- Assemble the 'new' statement to create+populate a new proc object
   rl.add(append ("new with {"));
   rl.add(appendf(" it.block=%s;", (is_string ? expression : quote(expression))));
   rl.add(append (" it.has_args=",(i_names.is_empty() ? "FALSE;":"TRUE;")));
   rl.add(appendf(" it.arg_names={%s};", str_join(i_names.apply(quote(it)), ";")));
   rl.add(appendf(" it.arg_types_and_defaults={%s};", str_join(i_types.apply(quote(it)), ";")));
   rl.add(append (" it.is_value_returning=(str_match(.block, \"/result/\") ? TRUE:FALSE);"));
   rl.add(append (" it.calc_props();"));
   rl.add(append ("}"));

   #ifdef DEBUG_MACROS {
      outf(">>> expansion of vlab_proc_def macro (is_string=%s) start <<<", is_string.as_a(string));
      for each in rl { out(it) };
      out(">>> expansion of vlab_proc_def macro end   <<<");
   };

   result = appendf("%s", str_join(rl, " "));
};

-- Struct to encapsulate the in/out data for a Proc call
struct vlab_proc_container_s {
   !output     : rf_value_holder;
   !input      : list of rf_value_holder;
   !output_rft : rf_type;
   
   -- Helper method to store a return value in this container's holder
   set_output(val: untyped) is {
      assert output_rft != NULL;
      output = output_rft.create_holder(val.unsafe())
   };
};

extend sn_util {
   -- Global helper object for the Proc in/out data; there is only one instance,
   -- so we support only one atomic operation (no TCMs etc).
   package !vlab_proc_container: vlab_proc_container_s;
};

'>
