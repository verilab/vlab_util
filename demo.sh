#!/bin/sh

export SPECMAN_PATH=".." 
specman -p "load examples/test_top; test; exit"

# clean
rm -f *.elog *.log *.*~
rm -rf cov_work

# end
