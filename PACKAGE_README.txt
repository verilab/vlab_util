* Title: Verilab e-Language Utility Library

* Name: vlab_util

* Version: 1.0.0

* Modified: 18-May-2014

* Category: utility

* Support:
  andre.winkelmann@verilab.com
  thorsten.dworzak@verilab.com

* Documentation:
  docs/ver12_awinkelmann_tdworzak_verilab_paper.pdf (CDNLive! Technical paper)
  docs/ver12_awinkelmann_tdworzak_verilab_presentation.pdf
  docs/dworzak_specman_anonymous_methodsFINAL20150713.pdf (blog article)
* Release notes:

* Demo:
  run demo.sh
  examples folder contains a few testcases which utilize the macros in this library

* Requires:
  specman 12.2 ..
  
* Todo
- Extend lambda macro to also recognize "return" as keyword for value-returning code blocks
- Check if possible that yield has more parameters than defined in Proc construction (currently handled as error)
